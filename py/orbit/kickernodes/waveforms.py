#!/usr/bin/env python

"""
This is not a parallel version! 
"""

import math
import random
import sys
from itertools import chain

class rootTWaveform:
	""" 
	This class has horizontal sqrt(time) waveform.
	"""
	def __init__(self, syncpart, lattlength, duration, startamp, endamp):
		self.name = "RootTWaveform"
		self.syncpart = syncpart
		self.lattlength = lattlength
		self.duration = duration
		self.startamp = startamp
		self.endamp = endamp
	
	def getKickFactor(self):
		time = self.syncpart.time()
		amp = self.startamp - self.endamp
		factor = amp * ( 1 - math.sqrt(time/self.duration)) + self.endamp
		return factor
	


class flatTopWaveform:
	""" 
	This class has the flat top (const) waveform.
	"""
	def __init__(self, amp):
		self.name = "FlatTopWaveform"
		self.amp = amp
	
	def getKickFactor(self):
		return self.amp


class RangeRootTWaveform:
	"""
	This class has the squareroot kikcer wave in a time range
	"""
	def __init__(self, syncPart, t1, t2, k, d):
		self.syncpart = syncPart
		self.starttime = t1
		self.endtime = t2
		self.startamp = k
		self.endamp = d

	def getKickFactor(self):
		time = self.syncpart.time()
		if time < self.starttime or time > self.endtime:
			return 0.0
		else:
			amp = self.startamp - self.endamp
			factor = amp * (1 - math.sqrt((time-self.starttime)/(self.endtime-self.starttime))) + self.endamp
			return factor

class RangeRootTWithFlattopWaveform:
	"""
	This class has the squareroot kikcer wave in a time range
	"""
	def __init__(self, syncPart, t1, t2, k, d):
		self.syncpart = syncPart
		self.starttime = t1
		self.endtime = t2
		self.startamp = k
		self.endamp = d

	def getKickFactor(self):
		time = self.syncpart.time()
		if time > self.endtime:
		    return 0.0
		elif time < self.starttime:
		    return self.startamp
		else:
		    amp = self.startamp - self.endamp
		    factor = amp * (1 - math.sqrt((time-self.starttime)/(self.endtime-self.starttime))) + self.endamp
		    return factor

class CustomWaveform:
	"""
	This class get the factor from customize curve
	"""
	def __init__(self, syncPart, t1, t2, timetuple, amptuple):
		self.syncpart = syncPart
		self.starttime = t1
		self.endtime = t2
                self.time = timetuple
                self.amp = amptuple

	def getKickFactor(self):
		time = self.syncpart.time()
		if time > self.endtime:
		    return 0.0
		elif time < self.starttime:
		    return 0.0
		else:
		    factor = self.interp(time, len(self.time)-1, self.time, self.amp)
		    return factor

        def interp(self, x, n_tuple, x_tuple, y_tuple):
	    """
	    Linear interpolation: Given n-tuple + 1 points,
	    x_tuple and y_tuple, routine finds y = y_tuple
	    at x in x_tuple. Assumes x_tuple is increasing array.
	    """
	    if x <= x_tuple[0]:
	    	y = y_tuple[0]
	    	return y
	    if x >= x_tuple[n_tuple]:
	    	y = y_tuple[n_tuple]
	    	return y
	    dxp = x - x_tuple[0]
	    for n in range(n_tuple):
	    	dxm = dxp
	    	dxp = x - x_tuple[n + 1]
	    	dxmp = dxm * dxp
	    	if dxmp <= 0:
	    		break
	    y = (-dxp * y_tuple[n] + dxm * y_tuple[n + 1]) /\
	    	(dxm - dxp)
	    return y

class RangeRootTWaveformForY:
	"""
	This class has the squareroot kikcer wave in a time range ,the amp starts with a low growth rate 
	"""
	def __init__(self, syncPart, t1, t2, k, d):
		self.syncpart = syncPart
		self.starttime = t1
		self.endtime = t2
		self.startamp = k
		self.endamp = d

	def getKickFactor(self):
		time = self.syncpart.time()
		if time < self.starttime or time > self.endtime:
			return 0.0
		else:
			A = (self.endamp-self.startamp)/(self.endtime*self.endtime-self.starttime*self.starttime)
			B = 2*(self.endamp-self.startamp)*self.starttime/(-self.endtime*self.endtime+self.starttime*self.starttime)
			C = -(self.startamp*self.endtime*self.endtime+self.endamp*self.starttime*self.starttime-2*self.startamp*self.starttime*self.starttime)/(-self.endtime*self.endtime+self.starttime*self.starttime)
			factor = A*time*time+B*time+C
			return factor